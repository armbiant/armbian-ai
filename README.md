# COH3 Advanced AI

Script-based AI improvements for a more challenging and human-like AI for Company of Heroes 3.

This mod is based on a long standing COH1 project - Back to Basics mod. Most of AI's combat is completely replaced by the custom system.

List of main features:

- Full control of all AI units:
	- Custom capture logic.
	- Custom large-scale movement logic.
	- Completely custom combat.
- Close in logic for assault units.
- Custom logic for all infantry abilities.
- Unblob functionality.
- Replaced cover taking logic.
- Vehicles will stay away from anti-tank.
- Vehicles can dive.
- Vehicles will retreat for repairs.
- Control of barrage units - smoke barrages, flares, general barrages.
- Special logic for HMG, AT, sniper units.
- Custom and complex retreat logic for all units.
- Danger avoidance, especially for non-combat units.
- Ability avoidance.
- "Attention" system, AI will make more mistakes if multiple go on at once.
- "Pullback" system, AI is more likely to attack concentrations of enemy in waves.
- Fixed USF veterancy.
- Fixed repairs logic.
- Extra game mode for fully customizing AI bonuses.
- Many more smaller tweaks and adjustments.

Steam workshop link: https://steamcommunity.com/sharedfiles/filedetails/?id=2942790265

Special thanks to Janne252 for the help with development and testing.
